import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import StateTutorial from './hooks/StateTutorial'
import ReducerTutorial from './hooks/ReducerTutorial'
import EffectTutorial from './hooks/EffectTutorial'
import RefTutorial from './hooks/RefTutorial'
import LayoutEffectTutorial from './hooks/LayoutEffectTutorial';
import ContextTutorial from './hooks/ContextTutorial';
import MemoTutorial from './hooks/MemoTutorial';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <div style={{marginLeft:"20px"}}>
      <StateTutorial />
      <br />
      <ReducerTutorial />
      <br />
      <EffectTutorial />
      <br />
      <RefTutorial />
      <br />
      <LayoutEffectTutorial />
      <br />
      <ContextTutorial />
      <br />
      <MemoTutorial />
    </div>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
