import React, {useState} from "react";

const StateTutorial  = () => {
    const [counter, setCounter] = useState(0);
    const [input, setInput] = useState(0);
    const increment = ()=>{
        setCounter(counter+1)
        console.log(counter);
    }
    const inputChange = (event)=>{
        setInput(event.target.value)
    }
    return (
        <div style={{border:"2px solid", borderRadius: "5px", padding: "20px", margin: "2px"}}>
            <h2>useState Hook Tutorial</h2>
            {counter}
            <button onClick={increment}>Increment</button>
            <input type="text" onChange={inputChange}  />
            <br />
            <br />
            {input}
        </div>
    );
}

export default StateTutorial;