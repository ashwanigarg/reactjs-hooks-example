import React, { useState, createContext } from 'react'

import User from './User';
import Login from './Login';

export const AppContext = createContext(null);

export default function ContextTutorial() {
    const [username, setUsername] = useState("");

  return (
    <div style={{border:"2px solid", borderRadius: "5px", padding: "20px", margin: "2px"}}>
    <AppContext.Provider value={{username, setUsername}}>
        <h2>ContextTutorial</h2>
        <Login  />
        <User  />
    </AppContext.Provider>
    </div>
  )
}
