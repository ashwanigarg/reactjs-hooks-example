import React, {useEffect, useState} from 'react'
import axios from 'axios'

function EffectTutorial() {
    const [data, setData] = useState("")
    const [counter, setCounter] = useState(0);
    const increment = ()=>{
        setCounter(counter+1)
        console.log(counter);
    }

    useEffect(()=>{
        console.log('Page rendered.');
        axios.get("https://jsonplaceholder.typicode.com/comments")
        .then((res)=>{
            setData(res.data[counter].email)
            console.log("API was called");
        })
    }, [counter])
    return (
<div style={{border:"2px solid", borderRadius: "5px", padding: "20px", margin: "2px"}}>
            <h2>EffectTutorial</h2>
            <button onClick={increment}>Increment</button>
            <h1>{counter} - {data}</h1>
        </div>
    )
}

export default EffectTutorial