import React, { useRef } from 'react'

export default function RefTutorial() {

    const inputRef = useRef(null);    
    const onReset = () => {
        inputRef.current.value = "";
        inputRef.current.focus()
    }
    const onClick = () => {
        console.log(inputRef.current.value);
    }
    return (
<div style={{border:"2px solid", borderRadius: "5px", padding: "20px", margin: "2px"}}>
            <h2>Ref Hooks Tutorial</h2>
            <input type="text" placeholder='Exam.....' ref={inputRef}/>
            <button onClick={onReset}>Reset</button>
            <button onClick={onClick}>Submit</button>
        </div>
    )
}
