import React, { useEffect, useLayoutEffect } from 'react'

function LayoutEffectTutorial() {
  useEffect(()=>{
    console.log('useEffect called after page rendered once.');
  },[])

  useLayoutEffect(()=>{
    console.log('useLayoutEffect is called before useEffect');
  },[])


  return (
    <div style={{border:"2px solid", borderRadius: "5px", padding: "20px", margin: "2px"}}>
        <h2>LayoutEffectTutorial</h2>
    </div>
  )
}
 
export default LayoutEffectTutorial