import React, {useReducer} from 'react'

const reducer = (state, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return {count: state.count+1, showText: state.showText}
      break;
    case 'TOGGLE':
      return {count: state.count, showText: !state.showText} 
      break;
    default:
      return state 
      break;
  }
}

export default function ReducerTutorial() {
  // dispatch function will be called whenever we want to change anything inside the state variable.
  const [state, dispatch] = useReducer(reducer, {
    count:0,
    showText: true
  })



  return (
    <div style={{border:"2px solid", borderRadius: "5px", padding: "20px", margin: "2px"}}>
      <h2>useReducer Hook Tutorial</h2>
      <h1>{state.count}</h1>
      <button
      onClick={()=>{
        dispatch({type: "INCREMENT"})
        dispatch({type: "TOGGLE"})
      }}
      >
        Click here
      </button>

      {state.showText && <p>This is a text</p>  }
    </div>
  )
}
